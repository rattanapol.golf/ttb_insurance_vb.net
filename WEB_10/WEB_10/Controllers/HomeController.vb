﻿Imports System.Web.Http

Public Class HomeController
    Inherits System.Web.Mvc.Controller

    Function Index() As ActionResult
        Dim Product_Interface = New Product_Interface()
        Dim pt = Product_Interface.Get_data()
        ViewBag.Data = pt.ToArray
        Return View()
    End Function

    Function Add() As ActionResult
        Return View()
    End Function


    Function SentData(<FromBody()> ByVal value As String)
        Dim v As String() = value.Split("|")
        Dim Product_Interface = New Product_Interface()
        Product_Interface.Post_Data(v(0), v(1), Convert.ToDecimal(v(2)), Convert.ToInt32(v(3)))
        Return "SUccess"
    End Function


End Class
