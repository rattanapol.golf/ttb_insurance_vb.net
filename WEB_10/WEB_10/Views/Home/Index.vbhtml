﻿@Code
    ViewData("Title") = "Home Page"
End Code

<div class="text-center">
    <a href="/Home/Add" class="btn-primary btn">ADD Data</a>
    <br />
    <br />
    <br />
    <div class="row">
        <table id="dt" class="table table-striped table-bordered" style="width:100%">
            <thead>
                <tr>
                    <th>ProductId</th>
                    <th>ProductName</th>
                    <th>PricePerUnit</th>
                    <th>StockQty</th>
                </tr>
            </thead>
            <tbody>
                @For Each item In ViewBag.Data
                    @<tr>
                        <td>@item.ProductId</td>
                        <td>@item.ProductName</td>
                        <td>@item.PricePerUnit</td>
                        <td>@item.StockQty</td>
                    </tr>
                Next
            </tbody>
        </table>
    </div>
</div>

@Section Scripts

    <script type="text/javascript">
        $(document).ready(function () {
        });
        function loadProductData() {
            $.ajax({
                url: '/api/product', // Replace with your API URL
                type: 'GET',
                dataType: 'json',
                success: function (data) {
                    populateDataTable(data);
                },
                error: function (xhr, status, error) {
                    console.log(error);
                }
            });
        }
        function populateDataTable(data) {
            var table = $('#dt').DataTable({
                data: data,
                columns: [
                    { data: 'ProductId' },
                    { data: 'ProductName' },
                    { data: 'PricePerUnit' },
                    { data: 'StockQty' }
                ]
            });
        }
    </script>
End Section
