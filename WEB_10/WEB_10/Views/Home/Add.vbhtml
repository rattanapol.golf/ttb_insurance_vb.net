﻿@Code
    ViewData("Title") = "Add Product"
End Code



<h2>Add</h2>
<div class="row">
    <div class="col-6">
        <label>Product Code</label>
        <input name="ProductCode" id="ProductCode" class="form-control" />
    </div>
</div>
<div class="row">
    <div class="col-6">
        <label>Product Name</label>
        <input name="ProductName" id="ProductName" class="form-control" />
    </div>
</div>
<div class="row">
    <div class="col-6">
        <label>Price</label>
        <input name="PricePerUnit" id="PricePerUnit" type="number" class="form-control" />
    </div>
</div>
<div class="row">
    <div class="col-6">
        <label>StockQty</label>
        <input name="StockQty" id="StockQty" type="number" class="form-control" />
    </div>
</div>

<div>
    <input class="btn btn-success" value="Submit" onclick="Save()" />
</div>



@Section Scripts

    <script type="text/javascript">
        function Save() {
            $.ajax({

                url: '../Home/SentData', // Replace with your API URL
                type: 'POST',
                data: {
                    "value": $("#ProductCode").val() + "|" + $("#ProductName").val() + "|" + $("#PricePerUnit").val() + "|" + $("#StockQty").val()
                },
                dataType: 'json',
                success: function (data)
                {
                    alert("Success");
                    window.location.replace("/Home");
                },
                error: function (xhr, status, error) {
                    alert("Success");
                    window.location.replace("/Home");
                }
            });
        }
    </script>
End Section
