﻿Imports System.Data.SqlClient

Public Class Product
    Public ProductId As Integer
    Public ProductCode As String = ""
    Public ProductName As String = ""
    Public PricePerUnit As Decimal
    Public StockQty As Integer

End Class

Public Class Product_Interface
    Private cont As SqlConnection = New SqlConnection()
    Private sql_h As Sql_Helper = New Sql_Helper()

    Public Function Get_data() As List(Of Product)

        Dim res As String = ""
        cont = sql_h.Cont(cont)
        cont.Open()
        Dim cmd As String = "Select [ProductId],[ProductCode],[ProductName],[PricePerUnit],[StockQty] from Product "
        Dim res_dt As List(Of Product) = New List(Of Product)()

        Using command As SqlCommand = New SqlCommand(cmd, cont)

            Using reader As SqlDataReader = command.ExecuteReader()
                Dim resultList As List(Of Dictionary(Of String, Object)) = New List(Of Dictionary(Of String, Object))()

                While reader.Read()
                    Dim dr As Product = New Product()

                    For i As Integer = 0 To reader.FieldCount - 1

                        If reader.GetName(i) = "ProductId" Then
                            dr.ProductId = Convert.ToInt32(reader.GetValue(i).ToString())
                        End If

                        If reader.GetName(i) = "ProductCode" Then
                            dr.ProductCode = reader.GetValue(i).ToString()
                        End If

                        If reader.GetName(i) = "ProductName" Then
                            dr.ProductName = reader.GetValue(i).ToString()
                        End If

                        If reader.GetName(i) = "PricePerUnit" Then
                            dr.PricePerUnit = reader.GetValue(i).ToString()
                        End If
                        If reader.GetName(i) = "StockQty" Then
                            dr.StockQty = reader.GetValue(i).ToString()
                        End If
                    Next

                    res_dt.Add(dr)
                End While

                reader.Close()
            End Using
        End Using

        Return res_dt
    End Function

    Public Function Post_Data(ByVal ProductCode As String, ByVal ProductName As String, ByVal PricePerUnit As Decimal, ByVal StockQty As Integer) As String
        Dim res As String = ""
        cont = sql_h.Cont(cont)
        cont.Open()
        Dim cmd As String = "INSERT INTO [dbo].[Product]([ProductCode],[ProductName],[PricePerUnit],[StockQty]) VALUES('" & ProductCode & "','" & ProductName & "'," & PricePerUnit & "," & StockQty & ")"
        Dim res_dt As List(Of Product) = New List(Of Product)()

        Using command As SqlCommand = New SqlCommand(cmd, cont)

            Using reader As SqlDataReader = command.ExecuteReader()
                res = "Success"
                reader.Close()
            End Using
        End Using

        Return res
    End Function
End Class
